import { BurgerEntity } from "src/entity/Burger";
import { BadRequestException, Injectable, NotFoundException } from "@nestjs/common";
import { InjectRepository } from "@nestjs/typeorm";
import { Repository } from "typeorm";
import { plainToClass } from 'class-transformer';
import { CreateBurgerDTO } from "./models/CreateBurgerDTO";
import { ShowBurgerDTO } from "./models/ShowBurgerDTO";

@Injectable()
export class BurgersService {

  public constructor(
    @InjectRepository(BurgerEntity)
    private readonly burgerRepo: Repository<BurgerEntity>) { }


  async getAllBurgers(): Promise<ShowBurgerDTO[]> {
    const allBurgers = await this.burgerRepo.find({
      where: {
        isDeleted: 0,
      },
    });
    return await plainToClass(ShowBurgerDTO, allBurgers, { excludeExtraneousValues: true });
  }


  async createBurger(burger: CreateBurgerDTO): Promise<ShowBurgerDTO> {
    const burgerToAdd: BurgerEntity = await this.burgerRepo.create(burger);
    const { name, ingredients, image_url, size } = burger;
    const createBurgerDTO: CreateBurgerDTO = {
      name,
      ingredients,
      image_url,
      size
    };
    if (Object.values(createBurgerDTO).some(prop => !prop)) {
      throw new BadRequestException('Invalid burger properties')
    }
    const savedBurger = await this.burgerRepo.save(burgerToAdd);

    return await plainToClass(ShowBurgerDTO, savedBurger, { excludeExtraneousValues: true });
  }

  async getBurgerById(burgerId: string): Promise<ShowBurgerDTO> {
    const burgerById = await this.burgerRepo.findOne({
      where: {
        id: burgerId,
        isDeleted: 0,
      },
    });

    if (!burgerById) {
      throw new NotFoundException('Burger with such ID not found!');
    }
    
    return burgerById
  }

}

import { Controller, Get } from '@nestjs/common';

@Controller()
export class AppController {

  @Get()
  root(): string {
    return `ver 0.1`;
  }
}

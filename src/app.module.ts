import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { ConfigModule } from './config/config.module';
import { ConfigService } from './config/config.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { BurgersModule } from './burgers/burgers.module';

@Module({
  imports: [BurgersModule,
    TypeOrmModule.forRootAsync({
    imports: [ConfigModule],
    inject: [ConfigService],
    useFactory: async (configService: ConfigService) => ({
      type: configService.dbType as any,
      host: configService.dbHost,
      port: configService.dbPort,
      username: configService.dbUsername,
      password: configService.dbPassword,
      database: configService.dbName,
      entities: ['./src/entity/*.ts'],
    }),
}),
],
  controllers: [AppController],
  providers: [],
})
export class AppModule {}

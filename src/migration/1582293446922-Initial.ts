import {MigrationInterface, QueryRunner} from "typeorm";

export class Initial1582293446922 implements MigrationInterface {
    name = 'Initial1582293446922'

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query("CREATE TABLE `burger` (`id` varchar(36) NOT NULL, `name` varchar(255) NOT NULL, `ingredients` varchar(255) NOT NULL, `image_url` varchar(255) NOT NULL, `size` varchar(255) NOT NULL, `isDeleted` tinyint NOT NULL DEFAULT 0, PRIMARY KEY (`id`)) ENGINE=InnoDB", undefined);
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query("DROP TABLE `burger`", undefined);
    }

}
